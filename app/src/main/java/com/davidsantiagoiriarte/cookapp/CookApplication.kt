package com.davidsantiagoiriarte.cookapp

import com.davidsantiagoiriarte.cookapp.di.AppComponent
import com.davidsantiagoiriarte.cookapp.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * @author davidiriarte on 23/03/20.
 */

open class CookApplication : DaggerApplication(){
    var appComponent: AppComponent? = null

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerAppComponent.builder()
            .application(this)
            .build()
        appComponent = component
        component.inject(this)
        return component
    }
}
