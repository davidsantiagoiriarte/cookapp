package com.davidsantiagoiriarte.cookapp.data.repositories

import com.davidsantiagoiriarte.cookapp.data.network.RecipeService
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import com.davidsantiagoiriarte.cookapp.domain.repositories.RecipeRepository
import java.lang.NullPointerException

/**
 * @author davidiriarte on 23/03/20.
 */
class RecipeRepositoryImpl(private val recipeService: RecipeService) : RecipeRepository {

    override suspend fun getRecipes(): List<Recipe> {
        return recipeService.getRecipes()
    }

    override suspend fun getRecipeById(id: Int): Recipe {
        return recipeService.getRecipeById(id)
    }

}
