package com.davidsantiagoiriarte.cookapp.data.network

import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import com.davidsantiagoiriarte.cookapp.util.RECIPES_EP
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author davidiriarte on 23/03/20.
 */

interface RecipeService {

    @GET("$RECIPES_EP/{id}")
    suspend fun getRecipeById(@Path("id") id: Int): Recipe


    @GET(RECIPES_EP)
    suspend fun getRecipes(): List<Recipe>

}
