package com.davidsantiagoiriarte.cookapp.di

import com.davidsantiagoiriarte.cookapp.data.network.RecipeService
import com.davidsantiagoiriarte.cookapp.data.repositories.RecipeRepositoryImpl
import com.davidsantiagoiriarte.cookapp.domain.repositories.RecipeRepository
import dagger.Module
import dagger.Provides

/**
 * @author davidiriarte on 23/03/20.
 */
@Module
class RepositoryModule {

    @Provides
    fun provideRecipeRepository(repositoryService: RecipeService): RecipeRepository {
        return RecipeRepositoryImpl(repositoryService)
    }
}