
package com.davidsantiagoiriarte.cookapp.di

import com.davidsantiagoiriarte.cookapp.recipes.RecipesListActivity
import com.davidsantiagoiriarte.cookapp.recipes.RecipesListActivityModule
import com.davidsantiagoiriarte.cookapp.recipes.detail.ItemDetailActivity
import com.davidsantiagoiriarte.cookapp.recipes.detail.ItemDetailActivityModule
import com.davidsantiagoiriarte.cookapp.splash.SplashActivity
import com.davidsantiagoiriarte.cookapp.splash.SplashActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(SplashActivityModule::class)])
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [(RecipesListActivityModule::class)])
    abstract fun bindRecipesListActivity(): RecipesListActivity

    @ContributesAndroidInjector(modules = [(ItemDetailActivityModule::class)])
    abstract fun bindItemDetailActivity(): ItemDetailActivity

}
