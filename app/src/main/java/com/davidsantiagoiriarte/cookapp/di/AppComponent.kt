
package com.davidsantiagoiriarte.cookapp.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [(AndroidSupportInjectionModule::class),
            (AppModule::class),
            (RepositoryModule::class),
            (InteractorsModule::class),
            (ServiceModule::class),
            (ActivityBuilder::class),
            (FragmentBuilder::class)
            ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Context): Builder

        fun build(): AppComponent
    }

}
