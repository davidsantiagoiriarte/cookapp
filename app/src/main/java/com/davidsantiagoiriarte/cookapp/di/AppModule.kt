
package com.davidsantiagoiriarte.cookapp.di

import com.davidsantiagoiriarte.cookapp.base.CoroutineContextProvider
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers

@Module
class AppModule {

    @Provides
    fun provideCoroutinesContextProvider() : CoroutineContextProvider{
      return CoroutineContextProvider(Dispatchers.Main, Dispatchers.IO)
    }

}
