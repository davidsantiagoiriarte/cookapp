package com.davidsantiagoiriarte.cookapp.di

import com.davidsantiagoiriarte.cookapp.base.Interactor
import com.davidsantiagoiriarte.cookapp.domain.interactors.GetRecipeByIdUseCase
import com.davidsantiagoiriarte.cookapp.domain.interactors.GetRecipesUseCase
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import com.davidsantiagoiriarte.cookapp.domain.repositories.RecipeRepository
import dagger.Module
import dagger.Provides

/**
 * @author davidiriarte on 23/03/20.
 */
@Module
class InteractorsModule {

    @Provides
    fun provideGetRecipeUseCase(recipeRepository: RecipeRepository): Interactor<List<Recipe>, String> {
        return GetRecipesUseCase(recipeRepository)
    }

    @Provides
    fun provideGetRecipeByIdUseCase(recipeRepository: RecipeRepository): Interactor<Recipe, Int> {
        return GetRecipeByIdUseCase(recipeRepository)
    }
}