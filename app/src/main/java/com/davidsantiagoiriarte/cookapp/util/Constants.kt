package com.davidsantiagoiriarte.cookapp.util

/**
 * @author davidiriarte on 23/03/20.
 */
const val BASE_URL = "http://gl-endpoint.herokuapp.com/"
const val RECIPES_EP = "recipes"
const val RECIPE_ID_EXTRA = "RECIPE_ID_EXTRA"
