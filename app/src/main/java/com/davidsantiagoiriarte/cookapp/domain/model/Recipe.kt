package com.davidsantiagoiriarte.cookapp.domain.model

/**
 * @author davidiriarte on 23/03/20.
 */
data class Recipe(
    val id: Int,
    val title: String,
    val rating: Int? = null,
    val image: String? = null,
    val instructions: String? = null
)
