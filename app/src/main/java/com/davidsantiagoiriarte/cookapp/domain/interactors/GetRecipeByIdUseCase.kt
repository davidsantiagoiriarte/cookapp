package com.davidsantiagoiriarte.cookapp.domain.interactors

import com.davidsantiagoiriarte.cookapp.base.Interactor
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import com.davidsantiagoiriarte.cookapp.domain.repositories.RecipeRepository

/**
 * @author davidiriarte on 23/03/20.
 */
class GetRecipeByIdUseCase(private val recipeRepository: RecipeRepository) :
    Interactor<Recipe, Int> {

    override suspend fun invoke(id: Int): Recipe {
        return recipeRepository.getRecipeById(id)
    }

}
