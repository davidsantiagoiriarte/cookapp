package com.davidsantiagoiriarte.cookapp.domain.repositories

import com.davidsantiagoiriarte.cookapp.domain.model.Recipe

/**
 * @author davidiriarte on 23/03/20.
 */
interface RecipeRepository {
    suspend fun getRecipes() : List<Recipe>
    suspend fun getRecipeById(id : Int) : Recipe
}
