package com.davidsantiagoiriarte.cookapp.domain.interactors

import com.davidsantiagoiriarte.cookapp.base.Interactor
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import com.davidsantiagoiriarte.cookapp.domain.repositories.RecipeRepository

/**
 * @author davidiriarte on 23/03/20.
 */
class GetRecipesUseCase(private val recipeRepository: RecipeRepository) : Interactor<List<Recipe>, String> {

    override suspend fun invoke(title: String): List<Recipe> {
       return recipeRepository.getRecipes().filter {
           it.title.toUpperCase().startsWith(title.toUpperCase())
       }
    }

}
