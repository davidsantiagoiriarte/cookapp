package com.davidsantiagoiriarte.cookapp.splash

import com.davidsantiagoiriarte.cookapp.base.BasePresenter
import com.davidsantiagoiriarte.cookapp.base.BaseView

/**
 * @author davidiriarte on 23/03/20.
 */
interface SplashContract {
    interface View : BaseView{
        fun navigateToRecipesActivity()
    }

    interface Presenter : BasePresenter<View>
}
