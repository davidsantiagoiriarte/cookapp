package com.davidsantiagoiriarte.cookapp.splash

import android.content.Intent
import android.os.Bundle
import com.davidsantiagoiriarte.cookapp.recipes.RecipesListActivity
import com.davidsantiagoiriarte.cookapp.R
import com.davidsantiagoiriarte.cookapp.base.BaseActivity
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashContract.View {

    @Inject
    lateinit var presenter: SplashContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter.bind(this)
        lifecycle.addObserver(presenter)
    }

    override fun navigateToRecipesActivity() {
        startActivity(Intent(this, RecipesListActivity::class.java))
        finish()
    }
}
