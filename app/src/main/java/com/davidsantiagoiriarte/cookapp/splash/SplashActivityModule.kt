package com.davidsantiagoiriarte.cookapp.splash

import com.davidsantiagoiriarte.cookapp.base.CoroutineContextProvider
import dagger.Module
import dagger.Provides

/**
 * @author davidiriarte on 23/03/20.
 */
@Module
class SplashActivityModule {

    @Provides
    fun providePresenter(coroutineContextProvider: CoroutineContextProvider): SplashContract.Presenter {
        return SplashPresenter(coroutineContextProvider)
    }
}