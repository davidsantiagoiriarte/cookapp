package com.davidsantiagoiriarte.cookapp.splash

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.davidsantiagoiriarte.cookapp.base.CoroutineContextProvider
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay

/**
 * @author davidiriarte on 23/03/20.
 */
class SplashPresenter(
    override val coroutineContextProvider: CoroutineContextProvider
) : SplashContract.Presenter {
    override var view: SplashContract.View? = null
    override val parentJob: Job = Job()
    private val delayTwoSeconds = 2000L

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun onCreateView() {
        launchJobOnMainDispatcher {
            delay(delayTwoSeconds)
            view?.navigateToRecipesActivity()
        }
    }
}
