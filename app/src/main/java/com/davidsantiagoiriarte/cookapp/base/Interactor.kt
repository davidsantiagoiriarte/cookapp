package com.davidsantiagoiriarte.cookapp.base

/**
 * @author davidiriarte on 23/03/20.
 */
interface Interactor<Response, Params> where Response : Any {

    suspend operator fun invoke(
        params: Params
    ): Response
}
