package com.davidsantiagoiriarte.cookapp.base

import dagger.android.DaggerActivity
import dagger.android.support.DaggerAppCompatActivity

/**
 * @author davidiriarte on 23/03/20.
 */
open class BaseActivity : DaggerAppCompatActivity() {
}
