package com.davidsantiagoiriarte.cookapp.base

import kotlin.coroutines.CoroutineContext

/**
 * @author davidiriarte on 23/03/20.
 */
data class CoroutineContextProvider(
    val mainContext: CoroutineContext,
    val backgroundContext: CoroutineContext
)
