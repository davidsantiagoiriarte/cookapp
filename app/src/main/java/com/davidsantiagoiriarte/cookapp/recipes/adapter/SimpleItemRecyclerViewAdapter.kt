package com.davidsantiagoiriarte.cookapp.recipes.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.davidsantiagoiriarte.cookapp.R
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import com.davidsantiagoiriarte.cookapp.recipes.RecipesListActivity
import com.davidsantiagoiriarte.cookapp.recipes.RecipesListContract
import com.davidsantiagoiriarte.cookapp.recipes.detail.ItemDetailActivity
import kotlinx.android.synthetic.main.item_list_content.view.*

class SimpleItemRecyclerViewAdapter(
    private val presenter: RecipesListContract.Presenter,
    private val values: List<Recipe>
    ) :
            RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {

        private val onClickListener: View.OnClickListener

        init {
            onClickListener = View.OnClickListener { v ->
                   presenter.openRecipe(v.recipeId.text.toString().toInt())
            }
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_list_content, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            position: Int
        ) {
            val item = values[position]
            holder.contentView.text = item.title
            holder.recipeId.text = item.id.toString()

            with(holder.itemView) {
                tag = item
                setOnClickListener(onClickListener)
            }
        }

        override fun getItemCount() = values.size

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val recipeId : TextView = view.recipeId
            val contentView: TextView = view.content
        }
    }
