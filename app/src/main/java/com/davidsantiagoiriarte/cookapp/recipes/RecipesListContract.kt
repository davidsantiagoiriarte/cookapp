package com.davidsantiagoiriarte.cookapp.recipes

import com.davidsantiagoiriarte.cookapp.base.BasePresenter
import com.davidsantiagoiriarte.cookapp.base.BaseView
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe

/**
 * @author davidiriarte on 23/03/20.
 */
interface RecipesListContract {
    interface View : BaseView{
        fun showRecipes(recipes : List<Recipe>)
        fun navigateToRecipeDetail(id : Int)
        fun showLoadingView()
        fun hideLoadingView()
        fun showError(error : String)
    }

    interface Presenter : BasePresenter<View>{
        fun loadRecipes(searchParameter : String)
        fun openRecipe(id : Int)
    }
}
