package com.davidsantiagoiriarte.cookapp.recipes

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.DividerItemDecoration
import com.davidsantiagoiriarte.cookapp.R
import com.davidsantiagoiriarte.cookapp.base.BaseActivity
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import com.davidsantiagoiriarte.cookapp.recipes.adapter.SimpleItemRecyclerViewAdapter
import com.davidsantiagoiriarte.cookapp.recipes.detail.ItemDetailActivity
import com.davidsantiagoiriarte.cookapp.util.RECIPE_ID_EXTRA
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_recipes_list.*
import kotlinx.android.synthetic.main.item_list.*
import javax.inject.Inject


class RecipesListActivity : BaseActivity(), RecipesListContract.View {

    @Inject
    lateinit var presenter: RecipesListContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipes_list)
        presenter.bind(this)
        lifecycle.addObserver(presenter)
        setupRecyclerView()
        presenter.loadRecipes("")
        etSearch?.addTextChangedListener {
            presenter.loadRecipes(it.toString())
        }
    }

    private fun setupRecyclerView() {
        rvRecipes.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    override fun showRecipes(recipes: List<Recipe>) {
        if (recipes.isEmpty()) {
            tvNoResult?.visibility = View.VISIBLE
            rvRecipes?.visibility = View.INVISIBLE
        } else {
            tvNoResult?.visibility = View.GONE
            rvRecipes?.visibility = View.VISIBLE
        }
        rvRecipes.adapter =
            SimpleItemRecyclerViewAdapter(
                presenter,
                recipes
            )
    }

    override fun navigateToRecipeDetail(id: Int) {
        startActivity(Intent(this, ItemDetailActivity::class.java).apply {
            putExtra(RECIPE_ID_EXTRA, id)
        })
    }

    override fun showLoadingView() {
        pbLoading?.visibility = View.VISIBLE
        tvNoResult?.visibility = View.GONE
        rvRecipes?.visibility = View.INVISIBLE
    }

    override fun hideLoadingView() {
        pbLoading?.visibility = View.GONE
    }

    override fun showError(error: String) {
        Snackbar.make(window.decorView, error, Snackbar.LENGTH_LONG)
            .show()
    }

}
