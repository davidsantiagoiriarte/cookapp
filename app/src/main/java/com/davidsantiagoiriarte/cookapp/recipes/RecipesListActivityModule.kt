package com.davidsantiagoiriarte.cookapp.recipes

import com.davidsantiagoiriarte.cookapp.base.CoroutineContextProvider
import com.davidsantiagoiriarte.cookapp.base.Interactor
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import dagger.Module
import dagger.Provides

/**
 * @author davidiriarte on 23/03/20.
 */
@Module
class RecipesListActivityModule {

    @Provides
    fun provideRecipesListPresenter(
        coroutineContextProvider: CoroutineContextProvider,
        getRecipesUseCase: Interactor<List<Recipe>, String>
    ): RecipesListContract.Presenter {
        return RecipesListPresenter(coroutineContextProvider, getRecipesUseCase)
    }
}