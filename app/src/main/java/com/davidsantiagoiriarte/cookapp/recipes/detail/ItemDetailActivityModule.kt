package com.davidsantiagoiriarte.cookapp.recipes.detail

import com.davidsantiagoiriarte.cookapp.base.CoroutineContextProvider
import com.davidsantiagoiriarte.cookapp.base.Interactor
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import dagger.Module
import dagger.Provides

/**
 * @author davidiriarte on 23/03/20.
 */
@Module
class ItemDetailActivityModule {

    @Provides
    fun provideRecipesListPresenter(
        coroutineContextProvider: CoroutineContextProvider,
        getRecipesByIdUseCase: Interactor<Recipe, Int>
    ): ItemDetailContract.Presenter {
        return ItemDetailPresenter(coroutineContextProvider, getRecipesByIdUseCase)
    }
}