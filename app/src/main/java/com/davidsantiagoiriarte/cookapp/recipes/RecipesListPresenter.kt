package com.davidsantiagoiriarte.cookapp.recipes

import com.davidsantiagoiriarte.cookapp.base.CoroutineContextProvider
import com.davidsantiagoiriarte.cookapp.base.Interactor
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext

/**
 * @author davidiriarte on 23/03/20.
 */
class RecipesListPresenter(
    override val coroutineContextProvider: CoroutineContextProvider,
    private val getRecipesUseCase: Interactor<List<Recipe>, String>
) :
    RecipesListContract.Presenter {

    override var view: RecipesListContract.View? = null
    override val parentJob: Job = Job()

    override fun loadRecipes(searchParameter: String) {
        view?.showLoadingView()
        launchJobOnMainDispatcher {
            try {
                val recipes =
                    withContext(coroutineContextProvider.backgroundContext){
                        getRecipesUseCase.invoke(searchParameter)
                    }
                view?.showRecipes(recipes)
            } catch (ex: Exception) {
                view?.showError(ex.toString())
            }
            view?.hideLoadingView()
        }
    }

    override fun openRecipe(id: Int) {
        view?.navigateToRecipeDetail(id)
    }


}