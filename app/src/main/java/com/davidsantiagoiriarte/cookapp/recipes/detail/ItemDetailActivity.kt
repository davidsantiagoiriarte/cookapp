package com.davidsantiagoiriarte.cookapp.recipes.detail

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.davidsantiagoiriarte.cookapp.recipes.RecipesListActivity
import com.davidsantiagoiriarte.cookapp.R
import com.davidsantiagoiriarte.cookapp.base.BaseActivity
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import com.davidsantiagoiriarte.cookapp.util.RECIPE_ID_EXTRA
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_item_detail.*
import javax.inject.Inject

class ItemDetailActivity : BaseActivity() , ItemDetailContract.View {

    @Inject
    lateinit var presenter : ItemDetailContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter.bind(this)
        lifecycle.addObserver(presenter)
        presenter.loadRecipe( intent.getIntExtra(RECIPE_ID_EXTRA,0))
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    navigateUpTo(Intent(this, RecipesListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun showLoading() {
        pbLoading?.visibility = View.VISIBLE
        ivRecipe?.visibility= View.GONE
        rbRate?.visibility = View.GONE
        tvDescription?.visibility = View.GONE
    }

    override fun hideLoading() {
        pbLoading?.visibility = View.GONE
        ivRecipe?.visibility= View.VISIBLE
        rbRate?.visibility = View.VISIBLE
        tvDescription?.visibility = View.VISIBLE
    }

    override fun showRecipe(recipe: Recipe) {
        Glide.with(this).load(recipe.image).into(ivRecipe);
        rbRate.rating = recipe.rating?.toFloat() ?: 0F
        tvDescription?.text = recipe.instructions
        supportActionBar?.title = recipe.title
    }
    override fun showError(error: String) {
        Snackbar.make(window.decorView, error, Snackbar.LENGTH_LONG)
            .show()
    }
}
