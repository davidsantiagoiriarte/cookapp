package com.davidsantiagoiriarte.cookapp.recipes.detail

import com.davidsantiagoiriarte.cookapp.base.CoroutineContextProvider
import com.davidsantiagoiriarte.cookapp.base.Interactor
import com.davidsantiagoiriarte.cookapp.domain.interactors.GetRecipeByIdUseCase
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext

/**
 * @author davidiriarte on 23/03/20.
 */
class ItemDetailPresenter(
    override val coroutineContextProvider: CoroutineContextProvider,
    private val getRecipeByIdUseCase: Interactor<Recipe, Int>
) : ItemDetailContract.Presenter {

    override var view: ItemDetailContract.View? = null
    override val parentJob: Job = Job()

    override fun loadRecipe(id: Int) {
       launchJobOnMainDispatcher {
           try {
               view?.showLoading()
               val recipe =   withContext(coroutineContextProvider.backgroundContext){
                   getRecipeByIdUseCase.invoke(id)
               }
               view?.showRecipe(recipe)
           } catch (ex:Exception){
               view?.showError(ex.toString())
           }
           view?.hideLoading()
       }
    }

}