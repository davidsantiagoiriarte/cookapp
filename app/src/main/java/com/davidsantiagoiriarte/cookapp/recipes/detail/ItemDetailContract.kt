package com.davidsantiagoiriarte.cookapp.recipes.detail

import com.davidsantiagoiriarte.cookapp.base.BasePresenter
import com.davidsantiagoiriarte.cookapp.base.BaseView
import com.davidsantiagoiriarte.cookapp.domain.model.Recipe

/**
 * @author davidiriarte on 23/03/20.
 */
interface ItemDetailContract{

    interface View:BaseView {
        fun showLoading()
        fun hideLoading()
        fun showRecipe(recipe : Recipe)
        fun showError(error : String)
    }
    interface Presenter : BasePresenter<View>{
        fun loadRecipe(id : Int)
    }
}